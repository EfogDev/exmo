$('#loginForm').on('submit', (e) => {
    e.preventDefault();

    $.ajax({
        url: '/login',
        data: {
            login: $('#email').val(),
            password: $('#password').val(),
        }
    }).then(handleResponse);
});

$('#registrationForm').on('submit', (e) => {
    e.preventDefault();

    $.ajax({
        url: '/register',
        data: {
            login: $('#register_login').val(),
            email: $('#register_email').val(),
            password: $('#register_password').val(),
            subscribe: false
        }
    }).then(handleResponse);
});

function handleResponse(response, a, b, c) {
    console.info(response, a,b,c);

    if (!response)
        return alert('Unknown error. Please, try again later.');

    if (typeof response.success === 'undefined')
        return alert('Unknown error. Please, try again later.');

    if (response.success === 0)
        return alert(response.error.replace(/^(.*?): /gi, ''));

    alert('Success!');
}