const bodyParser = require('body-parser');
const express = require('express');
const OAuthServer = require('express-oauth-server');

const app = express();

app.oauth = new OAuthServer({
  model: require('./core/oauth')
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(app.oauth.authorize());

app.post('/authenticate', app.oauth.authenticate());
app.post('/token', app.oauth.token());

app.use(function (req, res) {
  res.sendStatus(403);
});

app.listen(3000);