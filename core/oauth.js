const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Exmo = require('./exmo');

mongoose.model('OAuthTokens', new Schema({
  accessToken: {type: String},
  accessTokenExpiresOn: {type: Date},
  client: {type: Object},
  clientId: {type: String},
  refreshToken: {type: String},
  refreshTokenExpiresOn: {type: Date},
  user: {type: Object},
  userId: {type: String},
}));

mongoose.model('OAuthClients', new Schema({
  clientId: {type: String},
  clientSecret: {type: String},
  redirectUris: {type: Array}
}));

const OAuthTokensModel = mongoose.model('OAuthTokens');
const OAuthClientsModel = mongoose.model('OAuthClients');

module.exports.getAccessToken = function (bearerToken) {
  return OAuthTokensModel.findOne({accessToken: bearerToken}).lean();
};

module.exports.getClient = function (clientId, clientSecret) {
  return OAuthClientsModel.findOne({clientId: clientId, clientSecret: clientSecret}).lean();
};

module.exports.getRefreshToken = function (refreshToken) {
  return OAuthTokensModel.findOne({refreshToken: refreshToken}).lean();
};

module.exports.getUser = function (login, password) {
  return Exmo.login({login, password});
};

module.exports.saveToken = function (token, client, user) {
  const accessToken = new OAuthTokensModel({
    accessToken: token.accessToken,
    accessTokenExpiresOn: token.accessTokenExpiresOn,
    client: client,
    clientId: client.clientId,
    refreshToken: token.refreshToken,
    refreshTokenExpiresOn: token.refreshTokenExpiresOn,
    user: user,
    userId: user._id,
  });

  return new Promise(function (resolve, reject) {
    accessToken.save(function (err, data) {
      if (err) reject(err);
      else resolve(data);
    });
  }).then(function (saveResult) {
    const data = Object.assign({}, saveResult && typeof saveResult == 'object' ? saveResult.toObject() : saveResult);

    data.client = data.clientId;
    data.user = data.userId;

    return data;
  });
};