const request = require('request');

module.exports = new class Exmo {
    constructor() {
        this.LOGIN_ENDPOINT = 'http://root.flyber.net/exmo/userLogin';
        this.REGISTER_ENDPOINT = 'http://root.flyber.net/exmo/userNew';
    }

    post(endpoint, fields) {
        return new Promise((resolve, reject) => {
            request({
                uri: endpoint,
                method: 'POST',
                json: fields
            }, (error, response, body) => {
                error ? reject(error) : resolve(body.success !== undefined ? body : JSON.parse(body.text));
            })
        });
    }

    login({login, password}) {
        return this.post(this.LOGIN_ENDPOINT, {login, password});
    }

    register(fields) {
        return this.post(this.REGISTER_ENDPOINT, fields);
    }
};